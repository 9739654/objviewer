
Laboratorium
Java i programowanie w sieci internet
prowadzący: Łuaksz Lipka
grupa 2
autor: Mateusz Paluch

Przeglądarka plików .obj

Założenia:
Napisać program wyświetlający modele 3D zapisane w formacje Wavefront .obj
Implementacja tylko części standardu: odczytywanie współrzędnych wierzchołków,
ścian i nazwy modelu, ignorowanie komentarzy i pozostałych elementów.

Realizacja:
Program odczytuje z argumentów ścieżkę do pliku  .obj, parsuje plik,
ignoruje niezaimplementowane elementy i wyświetla w okienku model
i osie współrzędnych dla ułatwienia orientacji. Program przechwytuje wyjątki
(np. błędy w pliku .obj) i wypisuje komunikat w terminalu. Nie jest zaimplementowane
odczytywanie normalnych wierzchołków, dlatego wyświetlony model wydaje się
płaski.

Program korzysta z bibliotek natywnych i został przetestowany w systemie Linux.