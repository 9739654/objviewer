/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objviewer.controller;

import java.io.File;
import java.io.IOException;
import java.nio.FloatBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;
import objviewer.exception.LineParseException;
import objviewer.model.Model;
import objviewer.view.Renderer;
import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;


/**
 * An instance of this class is used to display 3D models on the screen.
 * @author Mateusz Paluch
 * @version 0.1
 */
public class ObjViewer {

	
	private static final int DEFAULT_WIDTH = 300;
	private static final int DEFAULT_HEIGHT = 300;

	
	private final ObjLoader loader;
	private Model model;

	// lighting start
	FloatBuffer matSpecular;
	FloatBuffer lightPosition;
	FloatBuffer whiteLight;
	FloatBuffer lModelAmbient;
	
	/**
	 * @param args the command line arguments. The first argument must be the path to a .obj file
	 * @throws java.lang.Exception
	 */
	public static void main(String[] args) throws Exception {
		if (!testArgs(args)) {
			Renderer.info("first argument must be a path to the .obj file");
			return;
		}
		ObjViewer.init();
		File file = new File(args[0]);
		
		new ObjViewer().open(file).start();
	}

	private static boolean testArgs(String[] args) {
		return args.length == 1;
	}

	public static void init() throws Exception {
		LwjglLibs.setNativePaths();
		Renderer.info(System.getProperty("java.library.path"));
	}
	
	/**
	 * Creates an instance of this class given a path to a .obj file from which to model will be
	 * loaded.
	 */
	public ObjViewer() {
		loader = new ObjLoader();
	}

	/**
	 * Opens a model given a path to the file which contains the model.
	 * @param file
	 * @return this
	 */
	public ObjViewer open(File file) {
		try {
			model = loader.from(file).load();
		} catch (LineParseException | IOException ex) {
			Renderer.error(ex);
		}
		return this;
	}

	/**
	 * Starts the main loop of the application.
	 * @return this
	 */
	public ObjViewer start() {
		initDisplay();
		initOpenGL();
		mainLoop();
		return this;
	}

	private void initDisplay() {
		try {
			Display.setDisplayMode(new DisplayMode(800, 600));
			Display.create();
		} catch (LWJGLException e) {
			Renderer.error(e);
			System.exit(0);
		}
	}
	
	private void initOpenGL() {
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GLU.gluPerspective(67.0f, (DEFAULT_WIDTH / (float) DEFAULT_HEIGHT), 0.1f, 100.0f);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		
		GL11.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		GL11.glClearDepth(1.0f);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glDepthFunc(GL11.GL_LEQUAL);
		GL11.glHint(GL11.GL_PERSPECTIVE_CORRECTION_HINT, GL11.GL_NICEST);
		
		initLightArrays();
		GL11.glShadeModel(GL11.GL_SMOOTH);
		GL11.glMaterial(GL11.GL_FRONT, GL11.GL_SPECULAR, matSpecular);				// sets specular material color
		GL11.glMaterialf(GL11.GL_FRONT, GL11.GL_SHININESS, 50.0f);					// sets shininess

		GL11.glLight(GL11.GL_LIGHT0, GL11.GL_POSITION, lightPosition);	// sets light position
		GL11.glLight(GL11.GL_LIGHT0, GL11.GL_SPECULAR, whiteLight);	// sets specular light to white
		GL11.glLight(GL11.GL_LIGHT0, GL11.GL_DIFFUSE, whiteLight);	// sets diffuse light to white
		GL11.glLightModel(GL11.GL_LIGHT_MODEL_AMBIENT, lModelAmbient);	// global ambient light 

		GL11.glEnable(GL11.GL_LIGHTING);	// enables lighting
		GL11.glEnable(GL11.GL_LIGHT0);		// enables light0

		GL11.glEnable(GL11.GL_COLOR_MATERIAL);	// enables opengl to use glColor3f to define material color
		GL11.glColorMaterial(GL11.GL_FRONT, GL11.GL_AMBIENT_AND_DIFFUSE);
	}
	
	private void initLightArrays() {
		matSpecular = BufferUtils.createFloatBuffer(4);
		matSpecular.put(1.0f).put(1.0f).put(1.0f).put(1.0f).flip();

		lightPosition = BufferUtils.createFloatBuffer(4);
		lightPosition.put(0).put(0).put(10.0f).put(0.0f).flip();

		whiteLight = BufferUtils.createFloatBuffer(4);
		whiteLight.put(1.0f).put(1.0f).put(1.0f).put(1.0f).flip();

		lModelAmbient = BufferUtils.createFloatBuffer(4);
		lModelAmbient.put(0.5f).put(0.5f).put(0.5f).put(1.0f).flip();
	}
	
	private void mainLoop() {
        while (running()) {
			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
			GL11.glMatrixMode(GL11.GL_MODELVIEW);
			GL11.glLoadIdentity();
			GLU.gluLookAt(10, -15, 10, 0, 0, 0, 0, 0, 1);
			render();
			Display.update();
			Display.sync(60);
		}
		Display.destroy();
    }
	
	private boolean running() {
		return !Display.isCloseRequested();
	}
	
	private void render() {
		Renderer.drawAxis();
		Renderer.draw(model);
	}
	
}
