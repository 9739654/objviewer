/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package objviewer.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import objviewer.model.Face;
import objviewer.model.Quad;
import objviewer.model.Triangle;
import objviewer.model.Vertex;

/**
 *
 * @author Mateusz Paluch
 * @version 0.1
 */
public class FaceParser {
	private List<Vertex> vertexList;
	private List<Vertex> normalList;
	
	private String input;
	private List<Integer> faceVertexIndices;
	private List<Integer> faceNormalIndices;
	private final List<Vertex> faceVertices;
	private final List<Vertex> faceNormals;
	
	private Face result;
	
	public FaceParser() {
		faceVertexIndices = new ArrayList<>();
		faceNormalIndices = new ArrayList<>();
		faceVertices = new ArrayList<>();
		faceNormals = new ArrayList<>();
	}
	
	/**
	 * v1
	 * v1/vt1
	 * v1/vt1/vn1
	 * v1//vn1 v2//vn2 v3//vn3
	 */
	private final Consumer<String> parseIndice = (String indice) -> {
		String[] sIndices = indice.split("/");
		switch (sIndices.length) {
			case 1:
				faceVertexIndices.add(Integer.parseInt(sIndices[0])-1);
				break;
			case 2:
				throw new UnsupportedOperationException();
			case 3:
				faceVertexIndices.add(Integer.parseInt(sIndices[0])-1);
				// texture vertex indice -- not supported
				faceNormalIndices.add(Integer.parseInt(sIndices[2])-1);
				break;
		}
	};
	
	public FaceParser vertices(List<Vertex> vertexList) {
		this.vertexList = vertexList;
		return this;
	}
	
	public FaceParser normals(List<Vertex> normalList) {
		this.normalList = normalList;
		return this;
	}
	
	public FaceParser line(String line) {
		clearLists();
		line = line.trim();
		if (line.startsWith("f")) {
			line = line.substring(1).trim();
		}
		this.input = line;
		return this;
	}
	
	public Face process(Consumer<Face> consumer) {
		Arrays.asList(input.split("\\s+")).stream().forEach(parseIndice);
		
		faceVertexIndices.stream().forEach(vi -> faceVertices.add(vertexList.get(vi)));
		faceNormalIndices.stream().forEach(ni -> faceNormals.add(normalList.get(ni)));
		
		Face f;
		switch (faceVertices.size()) {
			case 3:
				f = new Triangle(faceVertices, normalList);
				break;
			case 4:
				f = new Quad(faceVertices, normalList);
				break;
			default:
				throw new UnsupportedOperationException();
		}
		if (consumer != null)
			consumer.accept(f);
		return f;
	}
	
	private void clearLists() {
		faceVertexIndices.clear();
		faceNormalIndices.clear();
		faceVertices.clear();
		faceNormals.clear();
	}
}
