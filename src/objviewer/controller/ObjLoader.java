/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package objviewer.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import objviewer.exception.LineParseException;

import objviewer.model.Model;
import objviewer.model.Quad;
import objviewer.model.Triangle;
import objviewer.model.Vertex;
import objviewer.view.Renderer;

/**
 * An instance of this class is used to load 3D models from wavefront .obj files
 * @author Mateusz Paluch
 * @version 0.1
 */
public class ObjLoader {
	private final List<String> lines;
	private int readLines;
	private String lastLine;
	private boolean loaded;
	
	private final List<Vertex> vertices;
	private final List<Vertex> normals;
	private final List<Triangle> triangles;
	private final List<Quad> quads;
	private String name;
	
	private Model model;
	
	private final VertexParser vertexParser;
	private final FaceParser faceParser;
	
	/**
	 * Creates new instance of this class
	 */
	public ObjLoader() {
		lines = new ArrayList<>();
		vertices = new ArrayList<>();
		normals = new ArrayList<>();
		triangles = new ArrayList<>();
		quads = new ArrayList<>();
		vertexParser = new VertexParser();
		faceParser = new FaceParser();
		faceParser.normals(normals);
		faceParser.vertices(vertices);
	}
	
	/**
	 * Sets the name of the file containing a model
	 * @param file
	 * @return instance of this class
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public ObjLoader from(File file) throws FileNotFoundException, IOException {
		reset();
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			String line = br.readLine();
			while (line != null) {
				lines.add(line);
				line = br.readLine();
			}
		} catch (FileNotFoundException notFoundEx) {
			Renderer.error(notFoundEx);
		}
		
		return this;
	}
	
	private void reset() {
		lines.clear();
		vertices.clear();
		normals.clear();
		triangles.clear();
		quads.clear();
		
		loaded = false;
		model = new Model();
		model.quads = new ArrayList<>();
		model.triangles = new ArrayList<>();
	}
	
	/**
	 * After setting the filename, this method parses the file and loads the model
	 * @return 3D model
	 * @throws objviewer.exception.LineParseException
	 */
	public Model load() throws LineParseException {
		if (!loaded) {
			try {
				lines.stream().forEach(line -> parseLine(line));
				loaded = true;
			} catch (Exception e) {
				throw new LineParseException(readLines, lastLine);
			}
		}
		model.triangles = new ArrayList<>(triangles);
		model.quads = new ArrayList<>(quads);
		model.name = name;
		return model;
	}
	
	private void parseLine(String line) {
		readLines++;
		lastLine = line = line.trim();
		if (line.startsWith("#") || line.length() == 0)
			return;
		
		String[] split = line.split("\\s+", 2);
		String prefix = split[0];
		String content = split.length>1 ? split[1] : "";
		
		switch (prefix) {
			case "v":
				vertexParser.line(content).process(vertex -> vertices.add(vertex));
				break;
			case "vn":
				vertexParser.line(content).process(vertex -> normals.add(vertex));
				break;
			case "o":
				parseObject(content);
				break;
			case "f":
				faceParser.line(content).process(face -> {
					if (face instanceof Triangle) {
						triangles.add((Triangle)face);
					} else if (face instanceof Quad) {
						quads.add((Quad)face);
					}
				});
				break;
			default:
				ignore(content);
		}
	}
	
	private void parseObject(String input) {
		name = input;
	}
	
	private void ignore(String input) {
		
	}
	
}
