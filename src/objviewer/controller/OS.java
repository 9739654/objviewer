/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objviewer.controller;

/**
 * This enum is used to detect the current OS.
 * @author Mateusz Paluch
 * @version 0.1
 */
public enum OS {
	Mac, Unix, Windows, Other;
	
	private static final String os = System.getProperty("os.name").toLowerCase();
	
	public static OS kind() {
		if (os.contains("win"))
			return Windows;
		else if (os.contains("mac"))
			return Mac;
		else if (os.contains("nix") || os.contains("nux") || os.indexOf("aix") > 0)
			return Unix;
		else
			return Other;
	}
}
