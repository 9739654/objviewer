/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objviewer.controller;

import java.io.File;
import java.lang.reflect.Field;

/**
 * This class detects the current OS and sets proper paths.
 * @author Mati
 * @version 0.1
 */

public class LwjglLibs {
	
	public static void setNativePaths() throws Exception {
		File libs;
		String path;
		switch(OS.kind()) {
			case Windows:
				path = "libs/windows/";
				break;

			case Unix:
				path = "libs/linux";
				break;

			case Mac:
				path = "libs/macosx/";
				break;
				
			default:
				throw new Exception("Not supported OS");
		}
		libs = new File(path);
		System.setProperty("java.library.path", libs.getAbsolutePath());
		update();
	}
	
	private static void update() {
		Field fieldSysPath;
		try {
			fieldSysPath = ClassLoader.class.getDeclaredField("sys_paths");
			fieldSysPath.setAccessible(true);
			fieldSysPath.set(null, null);
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}
}
