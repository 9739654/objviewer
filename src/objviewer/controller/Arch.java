/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objviewer.controller;

/**
 * This enum is used to represent the architecture of current machine.
 * @author Mateusz Paluch
 * @version 0.1
 */
public enum Arch {
	x64, x86, Other;
	
	private static final String arch = System.getProperty("os.arch").toLowerCase();
	
	public static Arch kind() {
		Arch result;
		if (arch.contains("i386") || arch.contains("x86")) {
			result = x86;
		} else if (arch.contains("amd64")) {
			result = x64;
		} else {
			result = Other;
		}
		return result;
	}
	
}
