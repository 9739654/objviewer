/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package objviewer.controller;

import java.util.Optional;
import java.util.function.Consumer;

import objviewer.model.Vertex;

/**
 * Reads a Vertex from a .obj file line.
 * @author Mateusz Paluch
 * @version 0.1
 */
public class VertexParser {
	
	String input;
	
	/**
	 * Sets the line from which the vertex should be parsed.
	 * @param line
	 * @return an instance of this class
	 */
	public VertexParser line(String line) {
		line = line.trim();
		if (line.startsWith("v")) {
			line = line.substring(1).trim();
		}
		input = line;
		return this;
	}
	
	/**
	 * Parses the input and executes an operation given
	 * @param consumer the operation to execute after succesful parsing
	 * @return the parsed vertex
	 */
	public Vertex process(Consumer<Vertex> consumer) {
		String[] coords = input.split("\\s+");
		
		Vertex vertex = new Vertex();
		vertex.x = Float.parseFloat(coords[0]);
		vertex.y = Float.parseFloat(coords[1]);
		vertex.z = Float.parseFloat(coords[2]);
		
		if (consumer != null)
			consumer.accept(vertex);
		return vertex;
	}
}
