/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package objviewer.exception;

/**
 *
 * @author Mateusz Paluch
 * @version 0.1
 */
public class LineParseException extends Exception {

	int lineNumber;
	String line;
	
	public LineParseException(int lineNumber, String line) {
		super("Failed to parse line " + lineNumber + ": " + line);
		this.line = line;
		this.lineNumber = lineNumber;
	}
	
	public String getLine() {
		return line;
	}
	
}
