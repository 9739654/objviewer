/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package objviewer.view;

import java.util.logging.Level;
import java.util.logging.Logger;
import objviewer.controller.ObjViewer;
import objviewer.model.Model;
import objviewer.model.Quad;
import objviewer.model.Triangle;
import org.lwjgl.opengl.GL11;

/**
 *
 * @author Mateusz Paluch
 * @version 
 */
public class Renderer {
	public static void draw(Model model) {
		GL11.glColor3f(.3f, .3f, .3f);
		GL11.glBegin(GL11.GL_QUADS);
		for (Quad q : model.quads) {
			GL11.glVertex3f(q.a.x, q.a.y, q.a.z);
			GL11.glVertex3f(q.b.x, q.b.y, q.b.z);
			GL11.glVertex3f(q.c.x, q.c.y, q.c.z);
			GL11.glVertex3f(q.d.x, q.d.y, q.d.z);
		}
		GL11.glEnd();
		
		GL11.glBegin(GL11.GL_TRIANGLES);
		for (Triangle q : model.triangles) {
			GL11.glVertex3f(q.a.x, q.a.y, q.a.z);
			GL11.glVertex3f(q.b.x, q.b.y, q.b.z);
			GL11.glVertex3f(q.c.x, q.c.y, q.c.z);
		}
		GL11.glEnd();
	}
	
	public static void drawAxis() {
		GL11.glBegin(GL11.GL_LINES);
		GL11.glColor3f(1, 0, 0);	GL11.glVertex3f(0,0,0);	GL11.glVertex3f(10,0,0);
		GL11.glColor3f(0, 1, 0);	GL11.glVertex3f(0,0,0);	GL11.glVertex3f(0,10,0);
		GL11.glColor3f(0, 0, 1);	GL11.glVertex3f(0,0,0);	GL11.glVertex3f(0,0,10);
		GL11.glEnd();
	}

	public static void info(String message) {
		System.out.println(message);
	}

	public static void error(Exception e) {
		Logger.getLogger(ObjViewer.class.getName()).log(Level.SEVERE, null, e);
	}
}
