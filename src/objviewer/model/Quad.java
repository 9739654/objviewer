/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package objviewer.model;

import java.util.List;

/**
 * A face 
 * @author Mateusz Paluch
 * @version 
 */
public class Quad extends Triangle {
	
	public Vertex d;
	public Vertex dn;
	
	/**
	 * Creates a Quad with no vertices
	 */
	public Quad() {
		
	}
	
	/**
	 * Creates a Quad using vertices from the given triangle
	 * @param source 
	 */
	public Quad(Triangle source) {
		this.a = source.a;
		this.b = source.b;
		this.c = source.c;
		
		this.an = source.an;
		this.bn = source.bn;
		this.cn = source.cn;
	}

	/**
	 * Creates a Quad using vertices from given lists.
	 * @param vertexList
	 * @param normalList 
	 */
	public Quad(List<Vertex> vertexList, List<Vertex> normalList) {
		this(vertexList);
		readNormals(normalList);
	}
	
	/**
	 * Creates a Quad using vertices from given list.
	 * @param vertexList 
	 */
	public Quad(List<Vertex> vertexList) {
		super(vertexList);
		d = vertexList.get(3);
	}

	@Override
	protected void readNormals(List<Vertex> normals) {
		if (normals != null)
			switch(normals.size()) {
				case 4:
					dn = normals.get(3);
				case 3:
					super.readNormals(normals);
			}
	}
	
	@Override
	public String toString() {
		return "Q(" + a + ", " + b + ", " + c + ", " + d + ")";
	}
	
	
}
