/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package objviewer.model;

/**
 *
 * @author Mateusz Paluch
 * @version 0.1
 */
public class Vertex {
	public float x;
	public float y;
	public float z;

	/**
	 * Sets the vertex coordinates
	 * @param x
	 * @param y
	 * @param z
	 * @return this Vertex
	 */
	public Vertex set(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
		return this;
	}
	
	@Override
	public String toString() {
		return "V(" + x + ", " + y + ", " + z + ")";
	}
}
