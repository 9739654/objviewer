/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package objviewer.model;

import java.util.List;

/**
 * An instance of this class represents a 3D model.
 * @author Mateusz Paluch
 * @version 0.1
 */
public class Model {
	public List<Triangle> triangles;
	public List<Quad> quads;
	public String name;
	

}
