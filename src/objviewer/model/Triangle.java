/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package objviewer.model;

import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Mateusz Paluch
 * @version 0.1
 */
public class Triangle extends Face {
	public Vertex a, b, c;
	public Vertex an, bn, cn;
	
	/**
	 * Creates a Triangle with no vertices.
	 */
	public Triangle() {
		
	}
	
	/**
	 * Creates a Triangle using vertices from the given lists.
	 * @param vertexList
	 * @param normalList 
	 */
	public Triangle(List<Vertex> vertexList, List<Vertex> normalList) {
		this(vertexList);
		readNormals(normalList);
	}
	
	/**
	 * Creates a Triangle using vertices from the given list.
	 * @param vertexList 
	 */
	public Triangle(List<Vertex> vertexList) {
		a = vertexList.get(0);
		b = vertexList.get(1);
		c = vertexList.get(2);
	}
	
	protected void readNormals(List<Vertex> normals) {
		if (normals != null)
			switch (normals.size()) {
				case 3:
					cn = normals.get(2);
				case 2:
					bn = normals.get(1);
				case 1:
					an = normals.get(0);
			}
	}
	
	@Override
	public String toString() {
		return "T(" + a + ", " + b + ", " + c + ")";
	}
	
}
