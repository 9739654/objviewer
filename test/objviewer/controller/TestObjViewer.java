/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objviewer.controller;

import java.io.File;
import java.lang.reflect.Field;
import objviewer.model.Model;
import org.junit.Test;

/**
 *
 * @author Mateusz Paluch
 * @version 0.1
 */
public class TestObjViewer {
	
	@Test
	public void testMain() throws Exception {
		File input = new File("./diamond.obj");
		ObjViewer.init();
		ObjViewer app = new ObjViewer().open(input);
		
		Field field = ObjViewer.class.getDeclaredField("model");
		field.setAccessible(true);
		Model model = (Model)field.get(app);
		assert model != null;
	}

}
