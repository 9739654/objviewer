/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objviewer.controller;

import java.util.ArrayList;
import java.util.List;

import objviewer.model.Face;
import objviewer.model.Triangle;
import objviewer.model.Vertex;

import org.junit.Test;

/**
 *
 * @author Mateusz Paluch
 * @version 0.1
 */
public class TestFaceParser {
	
	public final List<Vertex> vertices = new ArrayList<>();
	{
		vertices.add(new Vertex().set(0, 0, 0));
		vertices.add(new Vertex().set(1, 1, 1));
		vertices.add(new Vertex().set(2, 2, 2));
		vertices.add(new Vertex().set(3, 3, 3));
	}
	
	public final String[] exampleFaceLines = {
		"f 15740/13814/15740 15743/13814/15743 15742/13814/15742",
		"f 1 2 3"
	};
	
	@Test
	public void testFaceLine() {
		Face result = new FaceParser()
				.vertices(vertices)
				.line(exampleFaceLines[1])
				.process(null);
		
		assert result instanceof Triangle;
		Triangle t = (Triangle)result;
		
		assert t.a.equals(vertices.get(0));
		assert t.b.equals(vertices.get(1));
		assert t.c.equals(vertices.get(2));
		assert t.an == null;
		assert t.bn == null;
		assert t.cn == null;
	}
}
