/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objviewer.controller;

import java.util.ArrayList;
import java.util.List;

import objviewer.model.Face;
import objviewer.model.Triangle;
import objviewer.model.Vertex;

import org.junit.Test;

/**
 *
 * @author Mateusz Paluch
 * @version 0.1
 */
public class TestVertexParser {
	
	public final String[] exampleVertexLines = {
		"v 0.0 0.0 0.0",
		"v 1.0 0.0 0.0",
		"v 0.0 2.5 0.5",
		"v 0.5 0.5 3.1"
	};
	public final String[] exampleFaceLines = {
		"f 15740/13814/15740 15743/13814/15743 15742/13814/15742",
		"f 1 2 3"
	};
	
	List<Vertex> vertices = new ArrayList<>();
	{
		vertices.add(new Vertex().set(0,0,0));
		vertices.add(new Vertex().set(1,0,0));
		vertices.add(new Vertex().set(0,2.5f,0.5f));
	}
	
	public TestVertexParser() {
	}

	@Test
	public void testVertexLine() {
		Vertex result = new VertexParser().line(exampleVertexLines[3]).process(null);
		assert result.x == .5f;
		assert result.y == .5f;
		assert result.z == 3.1f;
	}
}
