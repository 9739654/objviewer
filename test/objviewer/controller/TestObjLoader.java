/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objviewer.controller;

import java.io.File;
import java.io.IOException;
import objviewer.model.Model;
import org.junit.Test;

/**
 *
 * @author Mateusz Paluch
 * @version 0.1
 */
public class TestObjLoader {
	
	public TestObjLoader() {
	}
	
	@Test
	public void loadModel() throws IOException {
		File input = new File("./diamond.obj");
		Model result = new ObjLoader()
				.from(input)
				.load();
		
		assert result != null;
		assert result.name.equals("Object001");
		assert result.triangles.size() == 9;
		assert result.quads == null || result.quads.isEmpty();
	}
}
